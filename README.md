# hyperdic

*Hyper-fast* inter-process in-memory dictionary data structure
  * Read, Write in sub micro seconds
  * Optimized for Single Writer Multiple Reader(SWMR) pattern
